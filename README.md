# Detecting Patients Activities

## Activities includes
- Hand-wave recognition
- Person gasping detection
- Standing Person detection
- Detecting hands beating
- Detecting too much movement

## Contribute
- You can contribute to data collection & preprocessing and model building and training. 

For Data Collection : [Click here](https://gitlab.com/iotiotdotin/patient-monitoring-grp/detecting-patients-activities/-/issues?label_name%5B%5D=Dataset)

For Model & Training : [Click here](https://gitlab.com/iotiotdotin/patient-monitoring-grp/detecting-patients-activities/-/issues?label_name%5B%5D=Model+BT)
